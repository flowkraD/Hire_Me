# Hire Me

Made a Github account just because your college forced you to and never had any interest in FOSS? Well this is just the thing for you guys then. Don't let the recruiters see a blank profile, instead turn it into an innovative blank profile.

<br />
<br />

![alt text](https://github.com/JavaSkreeet/Hire_Me/blob/master/screenshot/Hire%20Me.jpeg)

<br />
<br />

At least now you have a profile worth displaying.

<br />

Just make an empty repository and add it's path and your OAuth Token in the index.js and you are good to go and make your Github account stand out.

<br />

### Warning

The script takes 231 days to complete the makeover so it is better deployed using Heroku. Also Github only shows the commits made in the past year so make sure youapply when your profile still looks classy.

### Contributions

Any Contributions and Suggestions are highly appreciated. Codebacks, even more so

# Cheers!

