var repo = {};
var githubName = "";//Your Repository Path user_name/repo_name
var githubToken = "";//Github OAuth toke from https://github.com/settings/tokens/new

//Importing all the required packages from js-github and authorising the cliet
require('js-github/mixins/github-db')(repo, githubName, githubToken);
require('js-git/mixins/create-tree')(repo);
require('js-git/mixins/mem-cache')(repo);
require('js-git/mixins/read-combiner')(repo);
require('js-git/mixins/formats')(repo);
const date = require('date-and-time');          //for extracting date and time
var run = require('gen-run');

let now = new Date();               //Date client
var day = date.format(now, 'ddd');  //Getting only the day in 3 letter format

//Hire Me Format - Contains the days on which a commit has to be made
var prt =   [1,2,3,4,5,6,7,11,18,25,29,30,31,32,33,34,35,43,44,45,46,47,48,49,57,58,59,60,61,62,63,64,67,68,71,74,76,78,79,80,81,84,92,93,94,95,96,97,98,
            99,102,105,106,109,112,113,116,119,120,123,126,141,142,143,144,145,146,147,149,157,165,171,177,183,184,185,186,187,188,189,197,198,199,200,201,
            202,203,204,207,210,211,214,217,218,221,224,225,228,231];

//Start on a saturday as day 0
if(day == "Sat"){
    run(function* () {
        while(true){
            //Getting all the required files and hashes
            var headHash = yield repo.readRef("refs/heads/master");
            var commit = yield repo.loadAs("commit", headHash);
            var tree = yield repo.loadAs("tree", commit.tree);
            var entry = tree["README.md"];
            var readme = yield repo.loadAs("text", entry.hash);
            var updates;
            //Counter to count days and the next array element to be accessed
            var cnt1=0,cnt2=0;
            
            //Counter 1 gets updated everyday once the day changes
            if(day != date.format(now, 'ddd')){
                day = date.format(now, 'ddd');
                cnt1++;
            }
            
            //Checking if there is to be a commit made on the particular day
            if(prt[cnt2] == cnt1){
                //Changing contents on README.md from Lower case to uppercase or vice versa on every iteration
                if(readme == readme.toLowerCase()){
                    updates = [
                        {
                            path: "README.md", 
                            mode: entry.mode,
                            content: readme.toUpperCase()
                        }
                    ];
                }
                else{
                    updates = [
                        {
                            path: "README.md",
                            mode: entry.mode,
                            content: readme.toLowerCase()
                        }
                    ];
                }
                        
                //Generating the comit
                updates.base = commit.tree;
                var treeHash = yield repo.createTree(updates);
                //Time to commit
                var commitHash = yield repo.saveAs("commit", {
                    tree: treeHash,
                    author: {
                    name: "",//put your name here
                    },
                    parent: headHash,
                    message: ""//commit message - think of something smart
                });

                //commiting to master 
                console.log("COMMIT", commitHash)
                yield repo.updateRef("refs/heads/master", commitHash);
                cnt2++;    
            }
            //Once this threshold is reached, the script has done it's job
            if(cnt2 == 231){
                console.log("YOU ARE ALL SET, NOW HURRY UP"); //One final motivational message
                break;
            }
        }
    });    
}

//WARNING:  This Program Executes for 231 days
//          If you are doing this to impress recruiters please do it so well before time
//          Github Tiles show commits for past year(roughly 365 days) so you will have about 4 months before all the hard work disappears
